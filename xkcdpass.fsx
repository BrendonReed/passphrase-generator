open System
open System.IO
open System.Security.Cryptography

printfn "Usage: fsharpi xkcdpass.fsx <numWords>"
let numWords = UInt16.Parse(fsi.CommandLineArgs.[1])
let wordFile = File.OpenRead("./words.txt")
let tWords = seq {
    use sr = new StreamReader (wordFile)
    while not sr.EndOfStream do
        yield sr.ReadLine()
}
let words = Seq.toList tWords
//let parsed = File.ReadAllLines("./words.txt")

//cryto strong random number < range up to 65kish
let rec crng (maxValue:uint16) : uint16 = 
    let fullRange = UInt16.MaxValue / maxValue
    printfn "max: %A" fullRange
    let rng16bit = 
        use rngCsp = new RNGCryptoServiceProvider()
        let randomNumber : byte[] = Array.zeroCreate 2
        rngCsp.GetBytes(randomNumber);
        BitConverter.ToUInt16(randomNumber, 0)
    let m = rng16bit
    //< NOT <= because that'd be one too many 0's
    match m < (fullRange * maxValue) with 
        | true -> m % maxValue
        | _ -> crng maxValue

let generatePassphrase (wordList:Collections.seq<string>) numWords = 
    let words = [1us..numWords] |> Seq.map (fun i ->
        let length = Seq.length words
        let index = crng (uint16 length)
        let word = wordList |> Seq.nth (int index)
        word)

    let s = Seq.fold (fun acc elem -> acc + elem + " ") "" words
    s
let passphrase = generatePassphrase words numWords
printfn "%A" passphrase 
